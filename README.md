### Ymazone

Ymazone | "Amazone" project for school in Angular 5 & NodeJs

## Installation
1. Clone the repo
2. ``` cd ymazon```
3. ``` cd ymazon-frontend ```
4. ``` npm install ```
5. ``` cd .. ```
6. ``` cd ymazon-backend ```
7. ``` npm install ```

## Config
1. For the backend rename the .env.example, rename .env and edit with your config
2. For the frontend edit the src/environements/environement.ts with your config

## Start project
1. In the backend folder run the command 'nodemon'
2. In the frontend folder run the command 'ng serve'

## Create some products
With Postman or other POST at the url : localhost:3000/api/products/new
with a body in json for example:
```
{
	"name": "Bravia",
	"brand": "Sony",
	"price": 699
} 

```

## Now play you can:
1. Register
2. Connect
3. See products
4. Add products to your basket
5. See your basket
6. Order the products in your basket
7. See your account and all your orders
8. Logout

## Infortunatly you can't:
1. Download your bill 

