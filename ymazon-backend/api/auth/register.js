const router = require('express').Router();
const User = require('../../models/user');
const validator = require('validator');

router.post('/', (req, res) => {

    if (validator.isEmail(req.body.email)) {
        let newUser = User({
            lastName: req.body.lastName,
            firstName: req.body.firstName,
            dateOfBirth: req.body.dateOfBirth,
            email: req.body.email,
            password: req.body.password
        });

        User.createUser(newUser, (err, user) => {
            if (err) {
                res.json({status: false, message: 'Failed to register user'});
            }

            res.json({status: true, message: 'User created successfully', user: {
                    lastName: user.lastName,
                    firstName: user.firstName,
                    dateOfBirth: user.dateOfBirth,
                    email: user.email
                }});
        });
    } else {
        res.json({status: false, message: 'Please provide a valid email and try again.'})
    }

});

module.exports = router;