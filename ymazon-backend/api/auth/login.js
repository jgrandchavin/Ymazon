const router = require('express').Router();
const User = require('../../models/user');
const validator = require('validator');
const jwt = require('jsonwebtoken');

router.post('/', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;

    if (validator.isEmail(email)) {
        User.getUserByEmail(email, (err, user) => {

            if (err) throw err;

            if (!user) {
                res.json({status: 500, message: 'Could not find a user with the email: ' + email + '.'});
            } else {
                User.comparePassword(password, user.password, (err, isMatch) => {

                    if (err) throw err;

                    if (!isMatch) {
                        res.json({status: 500, message: 'You provided a wrong password. Please try again.'});
                    } else {
                        // Generate token
                        const token = jwt.sign(user.toJSON(), process.env.SECRET);

                        res.json({status: true, message: user.firstName + ' has logged in successfully.', token:'JWT ' + token, user: {
                                lastName: user.lastName,
                                firstName: user.firstName,
                                dateOfBirth: user.dateOfBirth,
                                email: user.email,
                                userId: user._id
                            }});

                    }

                });
            }
        });

    } else {
        res.json({status: false, message: 'Please provide a valid email and try again.'});
    }
});

module.exports = router;