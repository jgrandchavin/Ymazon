const router = require('express').Router();
const Product = require('../../models/product');
const jwt = require('jsonwebtoken');

router.post('/new', (req, res) => {
    let newProduct = Product ({
        name: req.body.name,
        brand: req.body.brand,
        price: req.body.price
    });

    Product.createProduct(newProduct, (err, product) => {
        if (err) {
            res.json({status: false, message: 'Failed to create the product'});
        }
        res.json({status: true, message: 'Product created successfully', product: {
                name: product.name,
                brand: product.brand,
                price: product.price
            }});
    });
});

router.get('/', (req, res) => {
    Product.getAllProduct((err, products) => {
        if (err) {
            res.json({status: false, message: 'Failed get all products'});
        }
        res.json({status: true, products});
    });
});

router.get('/:id', (req, res) => {
    Product.getProductById(req.params.id, (err, product) => {
        if (err) {
            res.json({status: false, message: 'Failed to get product'});
        }

        res.json({status: true, product});

    });
});

module.exports = router;