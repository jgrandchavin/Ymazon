const router = require('express').Router();

router.use('/', require('./pdf'));

module.exports = router ;