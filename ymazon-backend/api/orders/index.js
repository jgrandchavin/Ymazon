const router = require('express').Router();

router.use('/', require('./orders'));

module.exports = router ;