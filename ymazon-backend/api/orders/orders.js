const router = require('express').Router();
const Order = require('../../models/order');

router.get('/:userId', (req, res) => {
    Order.getUserOrders(req.params.userId, (err, orders) => {
        if (err) {
            res.json({status: false, message: 'Failed get orders'});
        }
        res.json({status: true, orders});
    });
});

router.get('/find/:orderId', (req, res) => {
   Order.getOrder(req.params.orderId, (err, order) =>{
       if (err) {
           res.json({status: false, message: 'Failed get order'});
       }
       res.json({status: true, order});
   });
});

router.post('/new', (req, res) => {
    let newOrder = Order ({
        user: req.body.userId,
        products: req.body.products
    });

    Order.createOrder(newOrder, (err, order) => {
        if (err) {
            res.json({status: false, message: 'Failed to create the order'});
        }
        order.save();
        res.json({status: true, order});
    });
});

module.exports = router;
