
// Required constants
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
require('./config');
const cors = require('cors');


// Initialized app
const app = express();
const server = require('http').Server(app);

app.use(cors());

app.use(bodyParser.json());

app.use(morgan('dev'));

// Port
const port = process.env.PORT || 3000;

// Route
app.use('/api', require('./api'));

// Start server
server.listen(port, function() {
    console.log(`[INFO] Ymazon API started on port ${port}`);
});


