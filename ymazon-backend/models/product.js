const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const productSchema = new Schema({
    name: {
        type: String
    },
    brand: {
        type: String
    },
    price: {
        type: Number
    }
}, { usePushEach: true });

const Product = module.exports = mongoose.model('Product', productSchema);

module.exports.createProduct = function (newProduct, callback) {
    newProduct.save(callback);
};

module.exports.getAllProduct = function(callback) {
    Product.find({}, callback);
};

module.exports.getProductById = function (productId, callback) {
    Product.findById(productId, callback);
};