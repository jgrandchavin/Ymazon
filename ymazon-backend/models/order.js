const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const relationship = require('mongoose-relationship');

const orderSchema = new Schema({
    user: {
        type: Schema.ObjectId, ref: 'User', childPath: 'orders'
    },
   products: {
       type: [String],
       default: []
   }
}, { usePushEach: true });

orderSchema.plugin(relationship, {relationshipPathName: 'user'});

const Order = module.exports = mongoose.model('Order', orderSchema);

module.exports.createOrder = function (newOrder, callback) {
    newOrder.save(callback);
};

module.exports.getAllOrders = function(callback) {
    Order.find({}, callback);
};

module.exports.getUserOrders = function (userId, callback) {
    Order.find({user : userId}, callback);
};

module.exports.getOrder = function (orderId, callback) {
    Order.find({_id : orderId}, callback);
};


