const mongoose = require('mongoose');
const dotEnv = require('dotenv');

dotEnv.config();

mongoose.connection.openUri('mongodb://' + process.env.DB_HOST + ':' + process.env.DB_PORT + '/' + process.env.DB_NAME);

mongoose.connection.on('connected', () => {
    console.log('[INFO] Connected to DB ' + process.env.DB_NAME);
});

mongoose.connection.on('error', () => {
    console.log('[INFO] Could not connected to database');
});