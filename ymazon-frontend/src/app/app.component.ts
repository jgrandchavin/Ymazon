import { Component } from '@angular/core';
import { AuthenticationService} from "./_services/authentication.service";

import { CookieService} from "ngx-cookie-service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor (private authenticationService: AuthenticationService, private cookieService: CookieService) {}

  userIsConnected () {
    return (this.cookieService.get('token') !== "") ;
  }

  currentUser() {
    if (this.cookieService.get("currentUser") !== ""){
      return JSON.parse(this.cookieService.get("currentUser"));
    }
  }

  logout(){
    this.authenticationService.logout();
  }
}
