// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule }    from '@angular/common/http';
import { FormsModule } from '@angular/forms';

// Components
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from "./products/products.component";
import { BasketComponent } from './basket/basket.component';

// Services
import { UserService } from "./_services/user.service";
import { LoginComponent } from './login/login.component';
import { AuthenticationService } from "./_services/authentication.service";
import { CookieService } from "ngx-cookie-service";
import { ProductService } from "./_services/product.service";
import { OrderService } from "./_services/order.service";
import { AccountComponent } from './account/account.component';

@NgModule({
  declarations: [
    AppComponent,
    RegisterComponent,
    HomeComponent,
    LoginComponent,
    ProductsComponent,
    BasketComponent,
    AccountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [UserService, AuthenticationService, CookieService, ProductService, OrderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
