import { Component, OnInit } from '@angular/core';
import { CookieService} from "ngx-cookie-service";
import { ProductService } from "../_services/product.service";
import { OrderService} from "../_services/order.service";

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.css']
})
export class BasketComponent implements OnInit {

  constructor(private cookieService: CookieService, private productService: ProductService, private orderService: OrderService) { }

  productInBasket = [];

  basketOrdered: boolean;

  ngOnInit() {
    this.getAllProductFromBasket();
    this.basketOrdered = false;
  }

  getProduct (productId) {
    this.productService.getProduct(productId)
      .subscribe(
        data => {
          this.productInBasket.push(data['product']);
        }
      )
  }

  getAllProductFromBasket () {
    let self = this;
    if (this.cookieService.get("basket") !== "") {
      let productIds = JSON.parse(this.cookieService.get("basket"));

      productIds.forEach(function(productId){
        self.getProduct(productId);
      });
      this.calculateTotalPrice();
    }
  }

  calculateTotalPrice () {
    let basketsTotalPrice = 0;
    this.productInBasket.forEach(function(product){
      basketsTotalPrice += product.price;
    });
    return basketsTotalPrice;
  }

  createOrder () {
    let newOrder = {
        userId: JSON.parse(this.cookieService.get("currentUser")).userId,
        products: JSON.parse(this.cookieService.get("basket"))
    };

    this.orderService.create(newOrder).subscribe( data => {
      if (data['status'] === true ){
        this.basketOrdered = true;
        this.cookieService.delete("basket");
      }
    });
  }





}
