// Angular Dependencies
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// Environment Variables
import { environment } from "../../environments/environment";

@Injectable()
export class ProductService {

  constructor( private http: HttpClient ) { }

  getProduct(productId) {
    return this.http.get(environment.apiUrl + '/products/' + productId);
  }

  getAllProducts() {
    return this.http.get(environment.apiUrl + '/products');
  }

}
