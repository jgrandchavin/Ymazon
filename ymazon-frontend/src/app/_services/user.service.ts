// Angular Dependencies
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// Environment Variables
import { environment } from "../../environments/environment";

// User model
import { User } from "../_models/user";

@Injectable()
export class UserService {

  constructor( private http: HttpClient ) { }

  // Post - Register a User
  register(user: User){
    return this.http.post(environment.apiUrl + '/auth/register', user);
  }
}
