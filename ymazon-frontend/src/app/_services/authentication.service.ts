import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {environment} from "../../environments/environment";
import "rxjs/add/operator/map";

import { CookieService} from "ngx-cookie-service";

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient, private cookieService: CookieService) { }

  login(email: string, password: string) {
    return this.http.post<any>(environment.apiUrl + '/auth/login', {email: email, password: password})
      .map(user => {
        if (user && user.token) {
          this.cookieService.set("token", user.token);
          this.cookieService.set("currentUser", JSON.stringify(user.user));
        }
        return user;
      });
  }

  logout() {
    this.cookieService.delete("token");
    this.cookieService.delete("currentUser");
    this.cookieService.delete("basket");
  }
}
