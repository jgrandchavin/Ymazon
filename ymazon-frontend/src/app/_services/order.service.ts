import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Order } from '../_models/order';

// Environment Variables
import { environment } from "../../environments/environment";

@Injectable()
export class OrderService {

  constructor(private http: HttpClient) { }

  create(order: Order) {
    return this.http.post(environment.apiUrl + '/orders/new', order);
  }

  getUserOrders(userId: String) {
    return this.http.get(environment.apiUrl + '/orders/' + userId);
  };
}
