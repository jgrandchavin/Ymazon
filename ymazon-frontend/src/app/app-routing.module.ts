import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Component
import { RegisterComponent} from "./register/register.component";
import { HomeComponent} from "./home/home.component";
import { LoginComponent} from "./login/login.component";
import { ProductsComponent } from "./products/products.component";
import { BasketComponent } from "./basket/basket.component";
import { AccountComponent } from "./account/account.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'register', component: RegisterComponent},
  { path: 'login', component: LoginComponent},
  { path: 'products', component: ProductsComponent},
  { path: 'basket', component: BasketComponent},
  { path: 'account', component: AccountComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
