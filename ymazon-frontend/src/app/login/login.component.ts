import { Component, OnInit } from '@angular/core';

import { AuthenticationService} from "../_services/authentication.service";

import { Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user: any = {};
  authenticatedFailed: boolean;

  constructor(private authenticationService: AuthenticationService, private router: Router) { }

  ngOnInit() {
    this.authenticatedFailed = false;
  }

  login(){
    this.authenticationService.login(this.user.email, this.user.password)
      .subscribe(
        data => {
          if (data['status']) {
            this.router.navigate(['/']);
          } else {
            this.authenticatedFailed = true;
          }
        }
      )
  }

}
