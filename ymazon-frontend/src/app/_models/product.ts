export class Product {
  name: string;
  brand: string;
  price: number;
}
