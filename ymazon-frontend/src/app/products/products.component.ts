import { Component, OnInit } from '@angular/core';
import { ProductService } from "../_services/product.service";
import { CookieService } from "ngx-cookie-service";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  constructor(private productService: ProductService, private cookieService: CookieService) { }

  products = []; // All products
  currentUser = this.cookieService.get("currentUser");

  ngOnInit() {
    this.getAllProducts();
    this.products = Array.of(this.products);
  }

  // Get all products
  getAllProducts() {
    return this.productService.getAllProducts()
      .subscribe(
        data => {
          this.products = data['products'];
        }
    );
  }

  addProductToBasket (productId) {
    let basket = this.cookieService.get("basket");
    // If there is nothing in the basket
    if(basket === '') {
      this.cookieService.set("basket", JSON.stringify([]));
    }

    let tempsBasket = JSON.parse(this.cookieService.get("basket"));
    tempsBasket.push(productId);
    this.cookieService.set("basket", JSON.stringify(tempsBasket));
  }

}
