import { Component, OnInit } from '@angular/core';

import { UserService } from "../_services/user.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: any = {};

  constructor(private userService: UserService) { }

  registeredSuccessfully: boolean;

  ngOnInit() {
    this.registeredSuccessfully = null;
  }

  register() {
    this.userService.register(this.user)
      .subscribe(
        response => {
          this.registeredSuccessfully = response['status'];
        }
      )
  }

}
