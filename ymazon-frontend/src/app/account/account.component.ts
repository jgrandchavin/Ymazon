import { Component, OnInit } from '@angular/core';

import { CookieService } from "ngx-cookie-service";
import { OrderService } from "../_services/order.service";

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  constructor(private cookieService: CookieService, private orderService: OrderService) { }

  userOrders: any = [];

  ngOnInit() {
    this.getUserOrder(this.currentUser().userId);
  }

  currentUser() {
    if (this.cookieService.get("currentUser") !== ""){
      return JSON.parse(this.cookieService.get("currentUser"));
    }
  }

  getUserOrder(userId: String) {
    this.orderService.getUserOrders(userId).subscribe(
      data => {
        this.userOrders = data['orders'];
      }
    )
  }

}
